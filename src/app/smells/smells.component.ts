import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Smells } from "../interfaces/smells";

@Component({
    selector: "app-smells",
    templateUrl: "./smells.component.html",
    styleUrls: ["./smells.component.scss"]
})
export class SmellsComponent implements OnInit {

    smells: Array<Smells> = [];
    smellsGood: Array<string> = [];
    smellsBad: Array<string> = [];
    currentSmell: Smells = {
        id: 0,
        score: 0,
        theme: "",
        texte: ""
    };
    smellsImage: string = "";
    smellsBadImage: string = "";
    smellsBadIndex: number = 0;
    score: number = 0;
    smellsIndex: number = 0;
    pastSmells: Array<number> = [];
    buttons: boolean = true;
    diagnostic: boolean = false;

    constructor(private http: HttpClient) { }

    ngOnInit(): void {
        this.http.get<Array<Smells>>("assets/json/smells.json").subscribe(data => {
            this.smells = data;
            this.smellsIndex = Math.floor(Math.random() * this.smells.length);
            this.currentSmell = this.smells[this.smellsIndex];
            this.smellsImage = `/assets/smells/cropped/${ this.currentSmell.id }.jpg`;
            console.log(this.smellsImage);
        });
    }

    nextSmell(status: string, smell: Smells): void {
        if(this.score < 100) {

            this.smells.splice(this.smellsIndex, 1);
            this.smellsIndex = Math.floor(Math.random() * this.smells.length);

            this.currentSmell = this.smells[this.smellsIndex];
            this.smellsImage = `/assets/smells/cropped/${ this.currentSmell.id }.jpg`;

            if(status === "bad") {
                this.smellsBad.push(`/assets/smells/${ smell.id }.jpg`);
                this.score += smell.score;

                if(this.score >= 100) {
                    this.buttons = false;
                }

            } else {
                this.smellsGood.push(`/assets/smells/${ smell.id }.jpg`);
            }

        } else {
            this.buttons = false;
        }
    }

    toDiagnostic(): void {
        this.diagnostic = true;
        this.smellsBadImage = this.smellsBad[this.smellsBadIndex];
    }

    previousSmellsBad(): void {
        this.smellsBadIndex -= 1;
        this.smellsBadImage = this.smellsBad[this.smellsBadIndex]
    }

    nextSmellsBad(): void {
        this.smellsBadIndex += 1;
        this.smellsBadImage = this.smellsBad[this.smellsBadIndex]
    }

    restart(): void {
        this.smellsGood = [];
        this.smellsBad = [];
        this.smellsIndex = Math.floor(Math.random() * this.smells.length);
        this.currentSmell = this.smells[this.smellsIndex];
        this.smellsImage = `/assets/smells/cropped/${ this.currentSmell.id }.jpg`;
        this.smellsBadImage = "";
        this.smellsBadIndex = 0;
        this.score = 0;
        this.smellsIndex = 0;
        this.pastSmells = [];
        this.buttons = true;
        this.diagnostic = false;
    }
}

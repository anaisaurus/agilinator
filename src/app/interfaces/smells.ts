export interface Smells {
    id: number;
    score: number;
    theme: string;
    texte: string;
}

export interface DailyQuestions {
    depuis: string[];
    depuis_blocage: string[];
    blocage: string[];
    maintenant: string[];
    maintenant_blocage: string[];
    information: string[];
}

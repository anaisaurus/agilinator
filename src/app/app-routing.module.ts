import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DailyComponent } from "./daily/daily.component";
import { SmellsComponent } from "./smells/smells.component";

const routes: Routes = [
    { path: "daily", component: DailyComponent },
    { path: "", redirectTo: "/daily", pathMatch: "full" },
    { path: "smells", component: SmellsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

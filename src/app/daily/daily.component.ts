import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { DailyQuestions } from "../interfaces/daily";

@Component({
    selector: "app-daily",
    templateUrl: "./daily.component.html",
    styleUrls: ["./daily.component.scss"]
})
export class DailyComponent implements OnInit {

    questions: Array<string> = [];
    dailyQuestions: DailyQuestions = {
        depuis: [],
        depuis_blocage: [],
        blocage: [],
        maintenant: [],
        maintenant_blocage: [],
        information: []
    };

    constructor(private http: HttpClient) {}

    ngOnInit(): void {
        this.http.get<DailyQuestions>("assets/json/daily.json").subscribe(data => {
            this.dailyQuestions = data;
        });
    }

    generateDailyQuestions(): void {
        const numbers = [3, 4, 5];
        const number = numbers[Math.floor(Math.random() * numbers.length)];
        this.questions = [];
        this.questions.push(this.dailyQuestions.depuis[Math.floor(Math.random() * this.dailyQuestions.depuis.length)]);

        if(number === 3) {
            this.questions.push(this.dailyQuestions.blocage[Math.floor(Math.random() * this.dailyQuestions.blocage.length)]);
            this.questions.push(this.dailyQuestions.maintenant[Math.floor(Math.random() * this.dailyQuestions.maintenant.length)]);
        } else if(number === 4) {
            const blocages = [true, false];
            const blocage = blocages[Math.floor(Math.random() * blocages.length)];

            if(blocage) {
                this.questions.push(this.dailyQuestions.blocage[Math.floor(Math.random() * this.dailyQuestions.blocage.length)]);
                this.questions.push(this.dailyQuestions.maintenant[Math.floor(Math.random() * this.dailyQuestions.maintenant.length)]);
                this.questions.push(this.dailyQuestions.information[Math.floor(Math.random() * this.dailyQuestions.information.length)]);
            } else {
                this.questions.push(this.dailyQuestions.depuis_blocage[Math.floor(Math.random() * this.dailyQuestions.depuis_blocage.length)]);
                this.questions.push(this.dailyQuestions.maintenant[Math.floor(Math.random() * this.dailyQuestions.maintenant.length)]);
                this.questions.push(this.dailyQuestions.maintenant_blocage[Math.floor(Math.random() * this.dailyQuestions.maintenant_blocage.length)]);
            }
        } else if(number === 5) {
            this.questions.push(this.dailyQuestions.depuis_blocage[Math.floor(Math.random() * this.dailyQuestions.depuis_blocage.length)]);
            this.questions.push(this.dailyQuestions.maintenant[Math.floor(Math.random() * this.dailyQuestions.maintenant.length)]);
            this.questions.push(this.dailyQuestions.maintenant_blocage[Math.floor(Math.random() * this.dailyQuestions.maintenant_blocage.length)]);
            this.questions.push(this.dailyQuestions.information[Math.floor(Math.random() * this.dailyQuestions.information.length)]);
        }
    }

}

import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbMenuModule, NbCardModule,
    NbButtonModule, NbListModule } from "@nebular/theme";
import { NbEvaIconsModule } from "@nebular/eva-icons";

import { AppComponent } from "./app.component";
import { DailyComponent } from "./daily/daily.component";
import { SmellsComponent } from './smells/smells.component';

@NgModule({
    declarations: [
        AppComponent,
        DailyComponent,
        SmellsComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        NbThemeModule.forRoot({ name: "default" }),
        NbLayoutModule,
        NbSidebarModule.forRoot(),
        NbMenuModule.forRoot(),
        NbCardModule,
        NbEvaIconsModule,
        NbButtonModule,
        NbListModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }

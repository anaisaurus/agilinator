import { Component } from "@angular/core";
import { NbMenuItem } from "@nebular/theme";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"],
})
export class AppComponent {
    title = "Talon d'Agile";

    items: NbMenuItem[] = [{
        title: "Accueil",
        icon: "home-outline",
        link: "/"
    }, {
        title: "Outils",
        icon: "settings-2-outline",
        expanded: true,
        children: [
            {
                title: "Daily-inator",
                link: "daily"
            },
            {
                title: "Agile Smells-inator",
                link: "smells"
            }
        ]
    }, {
        title: "Blog",
        icon: "edit-outline"
    }];
}
